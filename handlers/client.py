from aiogram import types, Dispatcher
from create_bot import dp, bot
from keyboards import kb_client  
from aiogram.types import ReplyKeyboardRemove
from data_base import sqlite_db

# @dp.message_handler(commands=['start','help'])
async def comand_start(message : types.Message):
	try:	
		await bot.send_message(message.from_user.id, 'Приятного аппетита', reply_markup = kb_client)
		await message.delete()
	except:
		await message.reply('Общение с ботом через ЛС, напишите ему:\nhttps://t.me/DaNickebot')

# @dp.message_handler(commands=['Режим_работы'])
async def pizza_open_command(message : types.Message):
	await bot.send_message(message.from_user.id, 'Пт-Сб с 12:00 до 15:00')

# @dp.message_handler(commands=['Расположение'])
async def pizza_place_command(message : types.Message):
	await bot.send_message(message.from_user.id, 'ул. Акунаматата 1')#, reply_markup = ReplyKeyboardRemove())   удаляет клаву 



async def pizza_menu_command(message : types.Message):
	await sqlite_db.sql_read(message)



def register_handlers_client(dp : Dispatcher):
	dp.register_message_handler(comand_start, commands=['start', 'help'])
	dp.register_message_handler(pizza_open_command, commands=['Режим_работы'])
	dp.register_message_handler(pizza_place_command, commands=['Расположение'])
	dp.register_message_handler(pizza_menu_command, commands=['Меню'])