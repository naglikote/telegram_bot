from aiogram.types import ReplyKeyboardMarkup, KeyboardButton#, ReplyKeyboardRemove

b1 = KeyboardButton('/Режим_работы')
b2 = KeyboardButton('/Расположение')
b3 = KeyboardButton('/Меню')
#b4 = KeyboardButton('Поделиться номером', request_contact = True)
#b5 = KeyboardButton('Отправить где я', request_location = True)
# кнопки отправляют то, что в них написанно, исключение b4, b5 в которых можно написать что угодно

kb_client = ReplyKeyboardMarkup(resize_keyboard = True)
#kb_client = ReplyKeyboardMarkup(resize_keyboard = True, one_time_keyboard = True)
# аргумент resize_keyboard меняет размер кнопок под размер текста; one_time_keyboard после отправки команды с клавы, она удаляется 

kb_client.add(b1).add(b2).add(b3)#.row(b4, b5)
#kb_client.row(b1, b2, b3)
#kb_client.add(b1).add(b2).insert(b3)
# add добавляет кнопку в новой строке 
# row добавляет кнопки в одну строку 
# insert добовляет кнопку рядом с другой кнопкой, если есть место 

